let message = "Quelqu'un a les dates de l'ECA / contrôle partiel voire TP noté ?"

let () = print_endline message
;;

let rec renverse_ajoute (l1 : int list) (l2 : int list) : int list =
  match l1 with
  | [] -> l2
  | tete::queue -> renverse_ajoute queue (tete::l2)
;;
(*-- --*)
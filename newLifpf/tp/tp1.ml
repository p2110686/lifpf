(* 1.1.1 Types de base

evaluation comme calcul
*)

3*2+5/2;;

3.75 +. 17.45;;

"Adam" ^"Aysoy";;
(*on peut stoquer sur des variables *)
let maChaine = "text1" ^ "text2";;

a && b ;;
let a = true;;
let b = false;;
let c = false;;
true && false;;
true || false;;
b&&c;;
a||b;;
a || b && c;;(*le resultat est true d'ou il evalue && avant ||

*)
b&&c||a;;
3<2;;
3>2.0;;(*on ne peut evaluer float et int , car on evalue que meme type*)

(*1.1.2. Appels de fonction*)

float_of_int 3 +. 1.5;;
int_of_float 3.5 + 5;;(*il prend la partie entiere*)
(&&)true false;;
&& true false;;

2 < 3 && "b" < "ab";;
(&&) 2 <3 "b" < "ab";;(*error*)

(&&) (2 <3) ("b" < "ab");;

(*1.1.3. Variables*)

let ma_variable = sa_valeur in expression_qui_utilise_ma_variable;;

let x1 = 3.5 +. float_of_int 2
in x1 +. 3.0;;

let x1 = 3.5 +. float_of_int 2 in
let x2 = x1 *. x1 in
x2 *. 2.0 ;;(*tous leurs portee se terminent dans l'expression*)
x1;;
x2;;

(*1.1.4 Conditionnelle*)
(if true then 3 else 5) > 4;;
(*1.2. Définition de fonctions*)

let f x = x + 1;;
let discriminant0 a b c = b*b -4*a*c;;
let discriminant1 (a:float) (b:float) (c:float) = b*.b -. 4.0*.a*.c;;
let discriminant2 (a:float) (b:float) (c:float) : float = b*.b -. 4.0*.a*.c;;
discriminant0 4.3 2.0 1.0;;
discriminant0 4 2 1;;
discriminant1 4.3 2.0 1.0;;
discriminant2 4.3 2.0 1.0;;

(*### 1.3. Types somme

#### 1.3.1 Définitions de types somme*)
type couleur = Rouge | Jaune | Bleu | Violet | Orange | Vert | RJB ;;
Rouge;;
Rouge=Rouge;;
Rouge != Bleu;;
Bleu > Jaune;;
Bleu<Jaune;;


type mes_valeurs =
  UnInt of int
| DeuxInt of int * int
| UneChaine of string
;;


UnInt 3;;
UneChaine "toto";;
DeuxInt (3,4);;

type couleur = 
Rouge 
| Jaune 
| Bleu 
| Violet 
| Orange 
| Vert 
| RJB of int*int*int ;;

type couleur001 = 
Rouge 
| Jaune 
| Bleu 
| Violet 
| Orange 
| Vert 
| RJB (Rouge,,Bleu) ;;

(*c'est quoi la diffence entre int*int*int et ((DeuxInt)*int)*)
(3,5.6);;
(3,5,6);;
RJB 5 3 6;;

(*#### 1.3.2 Pattern matching*)

match c with
| Rouge -> "rouge"
| Bleu -> "bleu"
| Jaune -> "jaune"
| _ -> "mélange";;

let nom_couleur c =
  match c with
  | Rouge -> "rouge"
  | Bleu -> "bleu"
  | Jaune -> "jaune"
  |Violet -> "violet"
  | Orange -> "orange"
  | Vert -> "vert"
  | RJB _ -> "rouge jaune blanc"
  | _ -> "mélange"
;;
nom_couleur Rouge;;
nom_couleur Bleu;;
nom_couleur Violet;;
nom_couleur hs;;
nom_couleur Hs;;
nom_couleur _test001;;
nom_couleur Rouge Bleu;;
nom_couleur (Rouge Bleu Violet);;
nom_couleur Rouge Bleu Violet Orange;;
nom_couleur RJB;;

let nom_couleurSup c =
  match c with
  | Rouge -> "rouge"
  | Bleu -> "bleu"
  | Jaune -> "jaune"
  |Violet -> "violet"
  | Orange -> "orange"
  | Vert -> "vert"
  | RJB _ -> "rouge jaune blanc"
;;

let nom_couleur002 c =
  match c with
  _ ->0
  | Rouge -> "rouge"
  | Bleu -> "bleu"
  | Jaune -> "jaune"
  |Violet -> "violet"
  | Orange -> "orange"
  | Vert -> "vert"
  | RJB _ -> "rouge jaune blanc"
  | _ -> "mélange"
;;

let nom_couleur003 c =
  match c with
  |_ ->0
  | Rouge -> "rouge"
  | Bleu -> "bleu"
  | Jaune -> "jaune"
  |Violet -> "violet"
  | Orange -> "orange"
  | Vert -> "vert"
;;
let nom_couleur004 c =
  match c with
  |_ ->0
  | Rouge -> "rouge"
  | Bleu -> "bleu"
  | Jaune -> "jaune"
  |Violet -> "violet"
  | Orange -> "orange"
  | Vert -> "vert"
;;




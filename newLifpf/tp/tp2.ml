(*TP2*)

(*la fonction concatene*)  

let rec concatene (l1:int list) (l2:int list):int list = 
  match l1,l2 with
  | [], [] -> []
  | [], _ -> l2
  | _, [] -> l1
  |tete::queue, _ -> tete::(concatene queue l2);;

(* test fonction *)
concatene [1;2;3] [4;5;6];;

concatene [ 1; 2; 3 ] [ 4; 5; 6 ] = [ 1; 2; 3; 4; 5; 6 ];;
concatene [] [ 4; 5; 6 ] = [ 4; 5; 6 ];;
(*1.2 applatissement *)
let rec applatit (l:(int list) list):int list = 
  match l with
  | [] -> []
  | tete::queue -> concatene tete (applatit queue);;

(* test fonction *)
applatit [ [ 1; 2 ]; [ 3; 4; 5 ]; []; [ 6 ] ] = [ 1; 2; 3; 4;5; 6 ];;
applatit [ [ 1 ] ] = [ 1 ];;
applatit [ [ 1; 2 ]; [ 3; 4; 5 ]];;

(*applatit2 *)

let rec applatit2 (l:(int list) list) :int list = 
match l with
| [] -> []
| tete :: queue ->
    match tete with 
    | [] -> applatit2 queue (*tete int list et tete2 int ; queue c'est ma sous  list qui contient des int list premier partie  et queue2 snd part  )*)(* je pense que dans le queue2 y'a une sous liste vide *)
    | tete2 :: queue2 -> tete2 :: applatit2 (queue2 :: queue);;


    let rec applatitAysoy (l:(int list) list) :int list = 
      match l with
      | [] -> []
      | tete :: queue ->
          match tete with 
          | [] -> applatitAysoy queue 
          |tete2::_ -> tete2::(applatitAysoy queue);;
(* test fonction *)
applatitAysoy [ [ 1; 2 ]; [ 3; 4; 5 ]];;

(*error applatitAysoy *)

(*renver list *)
let rec renverse (l : int list ): int list =
  match l with 
  | [] -> []
  |tete::queue->renverse queue @ [tete];;
(* test fonction *)
renverse [1;2;3];;

(*renverse ajoute*)
let rec renverse_ajoute(l1 : int list ) (l2 : int list) : int list = 
  match l1 with 
  | [] -> l2
  | tete::queue -> renverse_ajoute queue (tete::l2);;

(* test fonction *)
renverse_ajoute [ 1; 2; 3 ] [ 4; 5; 6 ] = [ 3; 2; 1; 4; 5; 6];;
renverse_ajoute [ 1; 2; 3 ] [  ];;

(*renverse*)

let rec renverse2 (l : int list ): int list =
  let rec renverse_ajoute(l1 : int list ) (l2 : int list) : int list = 
    match l1 with 
    | [] -> l2
    | tete::queue -> renverse_ajoute queue (tete::l2) in
    renverse_ajoute l [];;


(* test fonction *)
renverse2 [1;2;3];;

(*3 tri par insertion *)
let rec insertion (x:int) (l:int list):int list = 
  match l with 
  | [] -> [x]
  | tete::queue -> 
      if x <= tete then x::l 
      else tete::(insertion x queue);;

(* test fonction *)
insertion 3 [ 1; 2; 4; 5 ] = [ 1; 2; 3; 4; 5 ];;
insertion 3 [ 1; 2; 3; 4; 5 ] = [ 1; 2; 3; 3; 4; 5 ];;
insertion 7 [ 1; 2; 3; 4; 5 ];;

(*tri insertion *)

let rec tri_insertion (l : int list ):int list =
  match l with
  | [] -> []
  | tete :: queue -> insertion tete (tri_insertion queue);;
(* test fonction *)

tri_insertion [ 1; 4; 2; 3 ];;
(* type somme resultat*)

type resultat = 
  | Absent
  |Present;;
let rec cherche (x:int) (l:int list) : resultat =
  match l with 
  | [] -> Absent
  |tete::queue ->
      if x = tete then Present
      else cherche x queue;;

(* test fonction *) 
cherche 3 [ 1; 2; 3; 4; 5 ] ;;
cherche 7 [ 1; 2; 3; 4; 5 ] ;;


(*calculatrice en notation polonaise *)
(*le pipe ça ne joue pas de mettre apres =, c'est juste si on veut faire decore *)
type binop = Plus | Moins | Mult | Div;;
type elt_expr = Op of binop | Cst of int;;







(*tp3*)
type arbre_bin = ABVide | ABNoeud of int * arbre_bin * arbre_bin;;

(*taille arbre *)

let rec taille_ab arbre = match arbre with
  | ABVide -> 0
  | ABNoeud(_,fg,fd) -> 1 + taille_ab fg + taille_ab fd;;

assert (taille_ab ABVide = 0);;


(*je ne sais pas pourquoi y'a erreur arbreTest1*)
let arbreTest1 = ABNoeud(5,ABNoeud(4,ABVide,ABVide ),ABNoeud(9 ,ABVide, ABNoeud(14, ABVide, ABVide) ));;
let arbreTest2 : arbre_bin = ABNoeud(3,ABVide,ABVide);;
let arbreTest3 = ABNoeud(8,ABVide,ABVide);;

let maTaille = taille_ab arbreTest3;;


(*produit arbre*)
let rec produit_ab arbre = match arbre with
  | ABVide -> 1 (*produit de 0 = 1 quand on remote la rec pour assenble*)
  | ABNoeud(v,fg,fd) -> v * produit_ab fg * produit_ab fd;;

assert (produit_ab ABVide = 1);;
(*test sur mon arbreTest1 et arbreTest3*)
assert (produit_ab arbreTest3 = 5*4*9*14);;
assert (produit_ab arbreTest2 = 3);;

(*arbre bin de recherche *)
let rec insere_arbre_bin_recherche (element : int ) ( arbre : arbre_bin) = 
match arbre with 
| ABVide -> ABNoeud(element , ABVide, ABVide)
|ABNoeud(valeur,fg,fd) 
  if (element < valeur) then 
    ABNoeud(valeur,insere_arbre_bin_recherche element fg,fd) 
else if (element > valeur) then 
  ABNoeud(valeur,fg,insere_arbre_bin_recherche element fd )
else arbre;;  

let abr1 = ABVide;;
let abr2 = insere_arbre_bin_recherche 5 abr1;;










